/*
 * _74hc595.h
 *
 * Created: 09.05.2018 7:26:12
 *  Author: Stitch
 */ 

#include <stdio.h>

#ifndef _74HC595_H_
#define _74HC595_H_

#define HC595_PORT  PORTC
#define HC595_DDR	DDRC

#define HC595_SER			PORTC7	// Data pin
#define HC595_CLCK			PORTC6	// Clock pin
#define HC595_LATCH			PORTC5	// Move data from shift register to output register
#define HC595_SH_REG_CLR	PORTC4	// Clear shift register
#define HC595_OE			PORTC3	// Output enable pin

#define HC595_SET_ONE	HC595_PORT |= (1 << HC595_SER)
#define HC595_SET_ZERO  HC595_PORT &= ~(1 << HC595_SER)

/************************************************************************/
/* Inicialize 74hc595                                                   */
/************************************************************************/
void hc595_init();

/************************************************************************/
/* Send data bit to shift register                                      */
/* @param bit	Data bit which will send                                */
/************************************************************************/
void hc595_send_bit(uint8_t);

/************************************************************************/
/* Send byte to shift register                                          */
/* @param byte	Byte which will send                                    */
/************************************************************************/
void hc595_send_byte(uint8_t);

/************************************************************************/
/* Send array of bytes to shift register                                */
/* @param dataArr	Reference to the byte's array                       */
/* @param len How much bytes need to send                               */
/************************************************************************/
void hc595_send_data(uint8_t*, uint8_t);

/************************************************************************/
/* Make clock pulse that to write a bit to the shift register			*/
/************************************************************************/
void hc595_strob_pulse();

/************************************************************************/
/* Latch entering data. Move to output bufer.							*/
/************************************************************************/
void hc595_latch();

/************************************************************************/
/* Clear shift register                                                 */
/************************************************************************/
void hc595_clear_shr();

#endif /* 74HC595_H_ */