/*
 * _74hc595.c
 *
 * Created: 09.05.2018 7:26:36
 *  Author: Stitch
 */ 

#include <avr/io.h>
#include "74hc595.h"

void hc595_init(){
	HC595_DDR |= ((1<<HC595_SER) | (1<<HC595_CLCK) | (1<<HC595_LATCH) | (1<<HC595_SH_REG_CLR) | (1<<HC595_OE));
	HC595_PORT &= ~((1<<HC595_SER) | (1<<HC595_CLCK) | (1<<HC595_LATCH) | (1<<HC595_SH_REG_CLR) | (1<<HC595_OE));
	hc595_clear_shr();
	hc595_latch();
}

void hc595_send_bit(uint8_t bit){
	if(bit){
		HC595_SET_ONE;
	}else{
		HC595_SET_ZERO;
	}
	hc595_strob_pulse();
}

void hc595_send_byte(uint8_t data){
	uint8_t  byteLen = 8;
	while(byteLen--){
		hc595_send_bit(data & 1);
		data >>= 1;
	}
}

void hc595_send_data(uint8_t* dataArr, uint8_t len){
	hc595_clear_shr();
	for (uint8_t i = 0; i < len; i++){
		hc595_send_byte(*(dataArr+i));
	}
	hc595_latch();
}

void hc595_strob_pulse(){
	HC595_PORT |= (1 << HC595_CLCK);
	HC595_PORT &= ~(1 << HC595_CLCK);
}

void hc595_latch(){
	HC595_PORT |= (1 << HC595_LATCH); 
	HC595_PORT &= ~(1 << HC595_LATCH);
}

void hc595_clear_shr(){
	HC595_PORT &= ~(1 << HC595_SH_REG_CLR); 
	HC595_PORT |= (1 << HC595_SH_REG_CLR);
}