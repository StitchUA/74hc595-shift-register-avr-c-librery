** sn74hc595 shift register**

Processor: 	ATMega16A
F_CPU:		8MHz

---

## Basic usage

1. Configure constatnts in 74hc595.h file as you need.
2. Call a hc595_init() function.
3. Call a hc595_send_data(uint8_t*, uint8_t) to send data to shift register.

For example:

Step 1:  
configuration of the 74hc595.h file

```C
#define HC595_PORT			PORTC  
#define HC595_DDR			DDRC  

#define HC595_SER			PORTC7	// Data pin  
#define HC595_CLCK			PORTC6	// Clock pin  
#define HC595_LATCH			PORTC5	// Move data from shift register to output register  
#define HC595_SH_REG_CLR	PORTC4	// Clear shift register  
#define HC595_OE			PORTC3	// Output enable pin  
```
Step 2: 

```C
	int main(){  
		uint8_t byteData = 0x0F;  
		uint16 myData = 0xE0C0;  
		uint8_t anotherDataArr[3] = {0xf00, 0xf00, 0xf00};  
	
		hc595_init();
	
		// Send to register one byte integer
		hc595_send_data(&byteData, 1);
	
		// Send to register 2 bytes indeger
		hc595_send_data((uint8_t*)(&myData), 2);
	
		// Send to register array
		hc595_send_data(anotherDataArr, 3);
	}
```
---

